# cli

> ODC Live's website

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

### start chrome without CORS

close all chrome instances:

"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --user-data-dir="tmp"
"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-site-isolation-trials --ignore-certificate-errors --user-data-dir="C:\tmp" --disable-web-security`
`"C:\Program Files\Google\Chrome\Application\chrome.exe" --disable-site-isolation-trials --ignore-certificate-errors --user-data-dir="C:\tmp" --disable-web-security`
